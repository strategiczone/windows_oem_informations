# windows_oem

This script add OEM Information and Logo

## How to:

#### Execute from local:
Download first this script and execute
```
powershell -exec bypass .\add_oem_informations.ps1
```


#### OR execute directly from Web
```
powershell -exec bypass 'iwr -UseBasicParsing https://gitlab.com/strategiczone/windows_oem_informations/raw/master/add_oem_informations.ps1|iex'
```
