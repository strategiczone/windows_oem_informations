<#
.SYNOPSIS
  This script insert Branding, OEM info and Custom wallpaper in system
.NOTES
	File Name  : add_oem_informations.ps1
	Author     : Strategic Zone
	Version    : 0.2
.LINK
	https://gitlab.com/strategiczone/windows_oem_informations
.EXAMPLE
  ### Local
  powershell -exec bypass .\add_oem_informations.ps1

  ### Web Delivery
  powershell -exec bypass "iwr -UseBasicParsing https://gitlab.com/strategiczone/windows_oem_informations/raw/master/add_oem_informations.ps1|iex"

#>

# Variables
$HelpCustomized = "true"
$SupportProvider = "Strategic Zone"
$SupportAppURL = "help@strategic.zone"
$SupportURL = "https://bugs.strat.zone"
$Manufacturer = "Strategic Zone"
$Model = "Custom by Strategic Zone"
$SupportHours = "08h00 - 20h00"
$SupportPhone = "+33 806 110 162"


$OSDISK = "C:"
$OEMLogo_URL = "https://gitlab.com/strategiczone/windows_oem_informations/raw/master/files/stratzone_logo.bmp"
$Wallpaper_URL = "https://gitlab.com/strategiczone/windows_oem_informations/raw/master/files/stratzone-wallpaper.png"
$OEMLogo_Name = "stratzone_logo.bmp"
$Wallpaper_Name = "stratzone-wallpaper.png"


# Clear OEM Inforamtion
Remove-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\OEMInformation" -Name "*" -Force

# Copy the OEM images
If (-not(Test-Path C:\Windows\System32\oobe\info\backgrounds)){New-item C:\Windows\System32\oobe\info\backgrounds -type directory}

<#
### Copy from local
copy-item .\$OEMLogo "$OSDISK\windows\system32"
#copy-item $PSScriptRoot\user.bmp "$OSDISK\ProgramData\Microsoft\User Account Pictures"
copy-item .\$OEMLogo "$OSDISK\windows\system32\oobe\info\"
copy-item .\$WALLPAPER "$OSDISK\windows\system32\oobe\info\backgrounds\"
copy-item .\$WALLPAPER "C:\Windows\Web\Screen\$WALLPAPER"
copy-item .\$WALLPAPER "C:\Windows\Web\Wallpaper\Windows\$WALLPAPER"
#>

iwr -UseBasicParsing $OEMLogo_URL -Out "$OSDISK\Windows\System32\$OEMLogo_Name"
iwr -UseBasicParsing $OEMLogo_URL -Out "$OSDISK\Windows\System32\oobe\info\$OEMLogo_Name"
iwr -UseBasicParsing $Wallpaper_URL -Out "$OSDISK\windows\System32\oobe\info\backgrounds\$Wallpaper_Name"
iwr -UseBasicParsing $Wallpaper_URL -Out "$OSDISK\Windows\Web\Screen\$Wallpaper_Name"
iwr -UseBasicParsing $Wallpaper_URL -Out "$OSDISK\Windows\Web\Wallpaper\Windows\$Wallpaper_Name"

# Make required registry changes
$strPath = "HKLM:\Software\Microsoft\Windows\CurrentVersion\OEMInformation"
$strPath2 = "HKLM:\Software\Microsoft\Windows\CurrentVersion\Authentication\LogonUI\Background"
$strPath3 = "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization"
$strPath4 = "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System"

Set-ItemProperty -Path $strPath -Name HelpCustomized -Value "$HelpCustomized"
Set-ItemProperty -Path $strPath -Name SupportProvider -Value "$SupportProvider"
Set-ItemProperty -Path $strPath -Name SupportAppURL -Value "$SupportAppURL"
Set-ItemProperty -Path $strPath -Name SupportURL -Value "$SupportURL"
Set-ItemProperty -Path $strPath -Name Manufacturer -Value "$Manufacturer"
Set-ItemProperty -Path $strPath -Name Model -Value "$Model"
Set-ItemProperty -Path $strPath -Name SupportHours -Value "$SupportHours"
Set-ItemProperty -Path $strPath -Name SupportPhone -Value "$SupportPhone"
Set-ItemProperty -Path $strPath -Name Logo -Value "C:\Windows\System32\$OEMLogo_Name"
Set-ItemProperty -Path $strPath2 -Name OEMBackground -value "1"

New-Item -Path HKLM:\Software\Policies\Microsoft\Windows -Name Personalization -Force
Set-ItemProperty -Path $strPath3 -Name LockScreenImage -value "C:\Windows\Web\Screen\$Wallpaper_Name"

New-Item -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Policies -Name System -Force
Set-ItemProperty -Path $strPath4 -Name Wallpaper -value "C:\Windows\Web\Wallpaper\Windows\$Wallpaper_Name"
Set-ItemProperty -Path $strPath4 -Name WallpaperStyle -value "2"

Write-Host "by Strategic Zone"
